const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');

const port = process.env.PORT || 5000;
const base = `${__dirname}/public`;

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");

    next();
});

app.use(express.static('public'));
//app.use(cookieParser());

app.get('/', (req, res) => {
    res.sendFile(`${base}/homepage.html`);
});

app.get('/login', (req, res) => {
    res.sendFile(`${base}/login.html`);
});

app.get('/register', (req, res) => {
    res.sendFile(`${base}/register.html`);
});

app.get('/forum', (req, res) => {
    res.sendFile(`${base}/forums.html`);
});

app.get('/forum/settings', (req, res) => {
    //const apiKey = req.cookies.api_key;

    res.sendFile(`${base}/forum-settings.html`);
});

app.get('/forum/thread/:categoryId', (req, res) => {
    res.sendFile(`${base}/forum-thread.html`);
});

app.get('/forum/:category/:topic/post', (req, res) => {
    //const apiKey = req.cookies.api_key;

    res.sendFile(`${base}/post-thread.html`);
});

app.get('/forum/:category/:topic', (req, res) => {
    //const apiKey = req.cookies.api_key;

    res.sendFile(`${base}/thread-list.html`);
});



//Error 404
// app.get('*', (req, res) => {
//     res.send('bye');
// });

//Web Listener\\
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});