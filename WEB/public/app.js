const API_URL = 'http://localhost:5000/api';
var categoryData = null;

$('#navbar').load('/navbar.html');

async function getUserInput(question) {
    const result = await Swal.fire({
        title: question,
        input: 'text',
        showCancelButton: true,
        inputValidator: (value) => {
            if (!value) {
                return 'You need to write something!'
            }
        }
    });

    return result.value;
}

function getDate(number) {
    const date = new Date(Number(number));
    return date.toString();
}

//Select Profile File Event
$("#profile").change((e) => {
    const file = e.target;

    $("#register-button").attr('disabled', '');

    //Convert file into base64 string
    var reader = new FileReader();
    reader.readAsBinaryString(file.files[0]);

    reader.onload = function() {
        localStorage.setItem('tempProfileImage', btoa(reader.result));
        $("#register-button").removeAttr('disabled');
    };
    reader.onerror = function() {
        console.error('ERROR: Failed to convert image to base64.');
    };


});

//Register Account
$('#register-button').click(() => {
    const displayName = $('#name').val();
    const email = $('#email').val();
    const password = $('#password').val();
    const confirmPassword = $('#confirm-password').val();
    const profileImage = localStorage.getItem('tempProfileImage');

    $.post(`${API_URL}/account/register`, { displayName, email, password, confirmPassword, profileImage })
        .then(response => {
            if (response.success == false) {
                Swal.fire('Invalid Operation', response.message, 'warning');
                return true;
            }

            window.location.href = '/login';
        })
        .catch(error => {
            Swal.fire('Error 404', `Failed to perform request. Please try again later!`, 'error');
        });
});

//Login Account
$('#login-button').click(() => {
    const username = $('#username').val();
    const password = $('#password').val();

    $.post(`${API_URL}/account/auth`, { username, password })
        .then(response => {
            if (response.success == false) {
                Swal.fire('Invalid Operation', response.message, 'warning');
                return true;
            }

            document.cookie = `api_key=${response.key}`;
            localStorage.setItem('api_key', response.key);
            localStorage.setItem('tempProfileImage', response.image);
            window.location.href = '/';
        })
        .catch(error => {
            Swal.fire('Error 404', `Failed to perform request. Please try again later!`, 'error');
        });
});

//Create Category Button
$('#create-category-button').click(async() => {
    const name = await getUserInput('What is the name of the new category?');

    $.post(`${API_URL}/forum/category`, {
            name,
            key: localStorage.getItem('api_key')
        })
        .then(response => {
            if (response.success == false) {
                Swal.fire('Invalid Operation', response.message, 'warning');
                return true;
            }

            Swal.fire('Category created', response.message, 'success');

            window.location.reload();
        })
        .catch(error => {
            Swal.fire('Error 404', `Failed to perform request. Please try again later!`, 'error');
        });
});

//Delete Category Button
$('#delete-category-button').click(async() => {
    if ($('#add-category-topic-button').attr('category') == undefined) {
        Swal.fire('Invalid Operation', 'Please select a category down below', 'warning');
        return;
    }

    //const name = await getUserInput('What is the name of the category to delete? (Case Senstive)');

    $.ajax({
        url: `${API_URL}/forum/category`,
        type: 'DELETE',
        data: {
            name: $('#add-category-topic-button').attr('category'),
            key: localStorage.getItem('api_key')
        },
        success: function(response) {
            if (response.success == false) {
                Swal.fire('Invalid Operation', response.message, 'warning');
                return true;
            }

            Swal.fire('Category Deleted', response.message, 'success');

            window.location.reload();
        }
    });
});

//Add Category Topic Button
$('#add-category-topic-button').click(async() => {
    const topic = await getUserInput('What is the name of the new topic?');
    const category = $('#add-category-topic-button').attr('category');

    if (category == undefined) {
        Swal.fire('Invalid Operation', 'Please select a category down below', 'warning');
        return;
    }

    if (topic == undefined) { return }

    $.post(`${API_URL}/forum/category/topic`, {
            name: category,
            topic,
            key: localStorage.getItem('api_key')
        })
        .then(response => {
            if (response.success == false) {
                Swal.fire('Invalid Operation', response.message, 'warning');
                return true;
            }

            Swal.fire('Topic Added', response.message, 'success');
            window.location.reload();
        })
        .catch(error => {
            Swal.fire('Error 404', `Failed to perform request. Please try again later!`, 'error');
        });
});

//Post Thread Button
$('#post-thread-button').click(async() => {
    const threadTitle = $('#thread-title').val();
    const category = window.location.toString().split('/')[4].replace(/%20/g, ' ');
    const topic = window.location.toString().split('/')[5].replace(/%20/g, ' ');
    console.log(topic);

    $.post(`${API_URL}/forum/thread`, {
            category,
            topic,
            key: localStorage.getItem('api_key'),
            title: threadTitle,
            postBody: $('#thread-body').html()
        })
        .then(response => {
            if (response.success == false) {
                Swal.fire('Invalid Operation', response.message, 'warning');
                return true;
            }

            Swal.fire('Thread Created', response.message, 'success');
            window.location = response.url;
        })
        .catch(error => {
            Swal.fire('Error 404', `Failed to perform request. Please try again later!`, 'error');
        });
});