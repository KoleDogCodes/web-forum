const mongoose = require('mongoose');

module.exports = mongoose.model('Accounts', new mongoose.Schema({
    apiKey: String,
    displayName: String,
    email: String,
    password: String,
    profileImage: String,
    settings: String
}));