const Accounts = require('./models/user');
const Categories = require('./models/category');
const Posts = require('./models/posts');
const Threads = require('./models/threads');
const crypto = require('crypto');

module.exports = {

    //Misc
    hash: function(secert, value) {
        var hash = crypto.createHmac('sha256', secert)
            .update(value)
            .digest('hex');

        //if (hash.length > 256) hash = hash.substr(0, 255);

        return hash;
    },

    strip: function(input) {
        return input.replace(/\W/g, '');
    },

    //Account Management
    authenicateAccount: async function(email, password) {
        var promise = Accounts.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        if (promiseValue.password == this.hash(email.toLowerCase(), password)) {
            return true;
        }

        return false;
    },

    registerAccount: async function(dispName, email, password, image) {
        email = email.toLowerCase();

        const newAccount = new Accounts({
            apiKey: this.hash(email, email + '.' + password),
            displayName: dispName,
            email,
            password: this.hash(email, password),
            profileImage: image,
            settings: JSON.stringify({
                bio: 'I just made an account!',
                permissions: 'CAN_POST,CAN_VIEW_POSTS'
            })
        });

        var promise = newAccount.save(err => {});

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? true : false;
    },

    apiKeyExists: async function(key) {

        var promise = Accounts.findOne({ apiKey: key });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getApiKey: async function(email) {
        var promise = Accounts.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue.apiKey;
    },

    getDisplayName: async function(email) {
        var promise = Accounts.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue.displayName;
    },

    accountExists: async function(email) {

        var promise = Accounts.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getProfileImage: async function(email) {
        var promise = Accounts.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue.profileImage;
    },

    getAccountPermissions: async function(key) {
        var promise = Accounts.findOne({ apiKey: key });

        const promiseValue = await promise;
        console.log(`${key} Permissions: ${JSON.parse(promiseValue.settings).permissions.split(',')}`);
        return JSON.parse(promiseValue.settings).permissions.split(',');
    },

    getAccountEmail: async function(key) {
        var promise = Accounts.findOne({ apiKey: key });

        const promiseValue = await promise;
        return promiseValue.email;
    },


    //Forum Category Managment
    categoryExists: async function(name) {
        var promise = Categories.findOne({ name: name });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    categoryIdExists: async function(name) {
        var promise = Categories.findOne({ id: name });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    generateCategoryId: async function(name) {
        const id = this.strip(name).replace(/ /g, '-');
        var promise = Categories.find({
            id: id,
        });

        const promiseValue = await promise;

        if (promiseValue.length == 0) { return id.toLowerCase() + '.0'; }

        return id.toLowerCase() + '.' + (promiseValue.length);
    },

    getCategoryId: async function(name) {
        var promise = Categories.findOne({ name: name });

        const promiseValue = await promise;

        return promiseValue.id;
    },

    getTopicId: async function(categoryId, topic) {
        var promise = Categories.findOne({ id: categoryId });

        const promiseValue = await promise;
        const topics = JSON.parse(promiseValue.topics);
        var id = NaN;

        for (var i = 0; i < Object.keys(topics).length; i++) {
            if (topics[i.toString()] == topic) {
                id = i;
                break;
            }
        }

        return id;
    },

    getCategories: async function() {
        var promise = Categories.find({});

        const promiseValue = await promise;

        if (promiseValue == null) { return {}; }

        const list = [];

        promiseValue.forEach(element => {
            list.push({ name: element.name, topics: element.topics, id: element.id });
        });

        return list;
    },

    getCategoryData: async function(name) {
        var promise = Categories.findOne({ name: name });

        const promiseValue = await promise;

        if (promiseValue == null) { return {}; }

        return JSON.parse(promiseValue.topics);
    },

    getCategoryId: async function(name) {
        var promise = Categories.findOne({ name: name });

        const promiseValue = await promise;

        return promiseValue.id;
    },

    addCategory: async function(name) {
        const newCategory = new Categories({
            id: await this.generateCategoryId(name),
            name: name,
            topics: JSON.stringify({})
        });

        var promise = newCategory.save(err => {});

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? true : false;
    },

    deleteCategory: async function(name) {
        var promise = Categories.deleteOne({ name: name });

        const promiseValue = await promise;

        return promiseValue;
    },

    addCategoryTopic: async function(name, topic) {
        var promise = Categories.findOne({ name: name }, (err, document) => {
            var topics = JSON.parse(document.topics);
            const keys = Object.keys(topics);

            if (keys.length == 0) {
                topics[0] = topic;
            } else {
                topics[Number(keys[keys.length - 1]) + 1] = topic;
            }

            document.topics = JSON.stringify(topics);

            document.save(err => {
                if (err) console.log("ERROR: Failed to update data.", err);
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    renameCategoryTopic: async function(name, topicId, topic) {
        var promise = Categories.findOne({ name: name }, (err, document) => {
            var topics = JSON.parse(document.topics);
            topics[topicId] = topic;

            document.topics = JSON.stringify(topics);

            document.save(err => {
                if (err) console.log("ERROR: Failed to update data.", err);
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    deleteCategoryTopic: async function(name, topicId) {
        var promise = Categories.findOne({ name: name }, (err, document) => {
            var topics = JSON.parse(document.topics);
            delete topics[topicId];

            document.topics = JSON.stringify(topics);

            document.save(err => {
                if (err) console.log("ERROR: Failed to update data.", err);
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },


    //Forum Thread Managment
    generateThreadId: async function(categoryId, topic, title) {
        const id = this.strip(title).replace(/ /g, '-');
        var promise = Threads.find({
            categoryId: categoryId,
            topicId: await this.getTopicId(categoryId, topic),
            title: title
        });

        const promiseValue = await promise;

        if (promiseValue.length == 0) { return id.toLowerCase() + '.0'; }

        return id.toLowerCase() + '.' + (promiseValue.length);
    },

    addThread: async function(key, categoryId, topic, title) {
        const id = await this.generateThreadId(categoryId, topic, title);

        const newThread = new Threads({
            threadId: id,
            categoryId: categoryId,
            topicId: topic,
            title: title,
            owner: await this.getAccountEmail(key),
            created_at: new Date().getTime(),
            settings: JSON.stringify({ views: 0, replies: 0 })
        });

        var promise = newThread.save(err => {});

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? id : null;
    },

    addPost: async function(key, threadId, postBody) {
        const email = await this.getAccountEmail(key);
        const time = new Date().getTime();

        const newPost = new Posts({
            threadId,
            body: postBody,
            email: email,
            created_at: time
        });

        var promise = newPost.save(err => {});

        const promiseValue = await promise;
        await this.incrementThreadReplies(threadId);
        await this.setThreadLatest(threadId, email, time, await this.getProfileImage(email));

        return promiseValue == null || promiseValue == undefined ? true : false;
    },

    getPosts: async function(threadId) {
        var promise = Posts.find({ threadId: threadId });

        const promiseValue = await promise;

        const list = [];

        for (var i = 0; i < promiseValue.length; i++) {
            const element = promiseValue[i];

            list.push({
                created_at: element.created_at,
                body: element.body,
                name: await this.getDisplayName(element.email),
                img: await this.getProfileImage(element.email)
            });
        }

        return list;
    },

    incrementThreadViews: async function(threadId) {
        var promise = Threads.findOne({ threadId }, (err, document) => {
            var stats = JSON.parse(document.settings);
            stats.views = stats.views + 1;

            document.settings = JSON.stringify(stats);

            document.save(err => {
                if (err) console.log("ERROR: Failed to update data.", err);
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    incrementThreadReplies: async function(threadId) {
        var promise = Threads.findOne({ threadId }, (err, document) => {
            var stats = JSON.parse(document.settings);
            stats.replies = Number(stats.replies) + 1;

            document.settings = JSON.stringify(stats);

            document.save(err => {
                if (err) console.log("ERROR: Failed to update data.", err);
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    setThreadLatest: async function(threadId, dispName, time, img) {
        var promise = Threads.findOne({ threadId }, (err, document) => {
            var stats = JSON.parse(document.settings);
            stats.latest_post = {
                name: dispName,
                time,
                img
            };

            document.topics = JSON.stringify(stats);

            document.save(err => {
                if (err) console.log("ERROR: Failed to update data.", err);
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getThreads: async function(categoryId, topicId) {
        var promise = Threads.find({ categoryId, topicId });

        const promiseValue = await promise;

        const list = [];

        for (var i = 0; i < promiseValue.length; i++) {
            const element = promiseValue[i];

            list.push({
                created_at: element.created_at,
                threadId: element.threadId,
                name: await this.getDisplayName(element.owner),
                img: await this.getProfileImage(element.owner),
                title: element.title,
                stats: JSON.parse(element.settings),
                latest_post: undefined
            });
        }

        return list;
    },
}